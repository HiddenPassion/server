const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const rules = { 
	type: String,
	required: true,
	trim : true 
};

const BookSchema = new Schema({
	title:	rules,
	description:	rules,
	author: rules,
	imageURL: rules,
	status: {
		type: String,
		trim: true,
		default: 'available'
	}
});

module.exports = mongoose.model('Books', BookSchema);
