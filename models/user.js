const mongoose = require('mongoose');
const bcrypt =  require('bcrypt-nodejs');

const Schema  = mongoose.Schema;

const rules = { 
  type: String,
  required: true,
  trim : true 
};

const UserSchema = new Schema({  
  email: {
    ...rules,
    lowercase: true,
    uniquie: true,
  },
  password: rules,
  profile: {
    firstName: rules,
    lastName: rules
  },
  role: {
    type: String,
    enum: [ 'Client', 'Admin'],
    default: 'Client'
	}
});

/*
const UserSchema = new Schema({  
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  profile: {
    firstName: { 
      type: String,
      required: true,
      trim : true 
    },
    lastName: { 
      type: String,
      required: true,
      trim : true 
    }
  },
  role: {
    type: String,
    enum: [ 'Client', 'Admin'],
    default: 'Client'
	}
});*/

UserSchema.pre('save', function(next) {  
  const user = this;
  const saltRounds = 5;

   bcrypt.genSalt(saltRounds, function(err, salt) {
     if (err) return next(err);

     bcrypt.hash(user.password, salt, null, function(err, hash) {
       if (err) return next(err);
       user.password = hash;
       next();
    });
   });
});

UserSchema.methods.comparePassword = function(incomingPassword, cb) {  
  bcrypt.compare(incomingPassword, this.password, function(err, isMatch) {
		if (err) { return cb(err); }
		
    cb(null, isMatch);
  });
}

module.exports = mongoose.model('User', UserSchema);  