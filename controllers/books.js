const Book = require('../models/books');
const mongoose = require('mongoose');
const config = require('../setting/config');
const _ = require('lodash');
const jwt = require('jsonwebtoken');

const  getUserByToken = (req) => {
	const token = req.headers.authorization.split(' ')[1]
	return jwt.verify(token, config.secret);
};

const isEmpty = (obj) => {
	for (var i in obj) {
			if (obj.hasOwnProperty(i)) {
					return false;
			}
	}
	return true;
}

const bookSendTemplate = ['status', '_id', 'title', 'description', 'author', 'imageURL'];

exports.getBooks = (req, res) => {	
	Book.find().then(result => {
		if(isEmpty(result)) throw new Error('Not Found'); //check

		let books = [];
		for ( let book of result) {
			books.push(_.pick(book, bookSendTemplate));
		}
		return res.status(200).json(books);
	}).catch(err => {
		return res.status(400).json(err);
	});
};


exports.getBook = (req, res, next) => {
	console.log(req.query.bookId);
	Book.findById(req.query.bookId).then(result => { ///need check id
			if(isEmpty(result)) throw new Error('Not Found'); 
			return res.status(200).json(_.pick(result, bookSendTemplate));
		})
		.catch(err => {
			return res.status(400).json('notFound');
		});
};

exports.removeBook = (req, res, next) => {
	const user = getUserByToken(req);
//	console.log(user);
	const isAdmin = 'Admin' == user.role;
	if(!isAdmin) res.status(400).send("You  don't have permission for this action");

	Book.findByIdAndRemove(req.body.bookId)//need check id
		.then(result => {
			console.log(result);
			if(isEmpty(result)) throw new Error('Not Found'); //check

			return res.status(200).send('Deleted Successfully')
		})
		.catch(err => {
			return res.status(400).json(err);
		})
};


exports.takeBook = (req, res, next) => {
	const user = getUserByToken(req);

	Book.findById(req.body.bookId) // need check id
		.then(result => {
			if (result.status == 'available') {
				Book.findByIdAndUpdate(req.body.bookId, {'status': user._id}) //check user and book id
					.then(result => {
						if(isEmpty(result)) throw new Error('Not Found'); //check

						return res.status(200).send('The book was taken successfully');
					})
					.catch(err => {
						return res.status(400).send('Something went wrong', err);
					})
			} else {
				throw new Error('This book not available yet');
			}
		})
		.catch(err => {
			return res.status(400).json(err);
		})
};

exports.returnBook = (req, res, next) => {
	const user = getUserByToken(req);
	Book.findById(req.body.bookId) // need check id
	.then(result => {
		// console.log('**********************');
		// console.log(result.status == user._id);
		// console.log('**********************');
			if (result.status === user._id) { //check user  id
				Book.findByIdAndUpdate(req.body.bookId, {'status': 'available'})
					.then(result => {
						//add check empty result
						res.status(200).send('The book was returned successfully')
					})
					.catch(err => {
						res.status(400).send('Something went wrong', err);
					})
			}
	})
	.catch(err => {
		res.status(400).json(err);
	});
};


exports.getUserTakenBooks =  (req, res, next) => {
	const user = getUserByToken(req);
	console.log('*******User ID********');
	console.log(user._id);
	console.log('**********************');
	Book.find({'status': user._id}) //check user id
		.then(result => {
			if(isEmpty(result)) throw new Error('Not Found'); //check
			console.log('******Searching result****');
			console.log(result);
			console.log('**********************');
			let books = [];
			for ( let book of result) {
				books.push(_.pick(book, bookSendTemplate));
			}
			return res.status(200).json(books);
		})
		.catch(err => {
			console.log('*****ERROR***********');
			console.log(err);
			console.log('**********************');
			return res.status(400).json(err);
		})
}

//find book by id
//Book.findById("5b4508f20e996812ccbc537a").then( book => console.log(book)).catch(err => console.log(err));

//get available book
//Book.find({'status': 'available'}).then(book => console.log(book));

// get all book
// Book.find().then(book => console.log(book));

//find by id and delele one
//Book.findByIdAndRemove("5b4508f20e996812ccbc537a").then(book => console.log(book)).catch(err => console.log(err));


//change field by id
//Book.findByIdAndUpdate("5b4508f20e996812ccbc537d", {'title': 'Changed'}).then(book => console.log(book));
//Book.findById('5b4508f20e996812ccbc537d').then(book => console.log(book));

