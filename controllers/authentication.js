const jwt = require('jsonwebtoken');
//const crypto = require('crypto');

const User = require('../models/user');
const config = require('../setting/config');

function generateToken(user) {
	return jwt.sign(user, config.secret, { expiresIn: 10000})
}

function setUserInfo(req) {  
  return {
    _id: req._id,
    firstName: req.profile.firstName,
    lastName: req.profile.lastName,
    email: req.email,
    role: req.role,
	};
}

// login route
exports.login = function(req, res, next) {

  let userInfo = setUserInfo(req.user);
  
  res.status(200).json({
    token: generateToken(userInfo),
    user: userInfo
  });
}


// registration route
exports.register = function(req, res, next) {    

  const email = req.body.email;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const password = req.body.password;

  if (!email) {
    return res.status(400).send({ error: 'You must enter an email address.'});
  }

  if (!firstName || !lastName) {
    return res.status(400).send({ error: 'You must enter your full name.'});
  }

  if (!password) {
    return res.status(400).send({ error: 'You must enter a password.' });
  }

  User.findOne({ email: email })
    .then(existingUser => {

      // if (existingUser) {
      //   return res.status(422).send({ error: 'That email address is already in use.' });
      // }

      let user = new User({
        email: email,
        password: password,
        profile: { firstName: firstName, lastName: lastName }
      });

      user.save()
        .then(user => {
          let userInfo = setUserInfo(user);
  
          res.status(201).json({
            token: generateToken(userInfo),
            user: userInfo
          });
        })
        .catch( err => next(err) )
      })
      .catch( err => next(err) );

}
