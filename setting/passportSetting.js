const passport =  require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

const User = require('../models/user');
const config = require('./config');

const localOptions = { usernameField: 'email' }; 

const localLogin = new LocalStrategy(localOptions, function(email, password, done) {  
	User.findOne({ email: email })
		.then(user => {
    	if(!user) { return done(null, false, { error: 'User with this email is not exist' }); }

    	user.comparePassword(password, function(err, isMatch) {
      if (err) { return done(err); }
      if (!isMatch) { return done(null, false, { error: "Invalid passport" }); }
      return done(null, user);
    });
		})
		.catch(err => done(err));
});

const jwtOptions = {   
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), 
  secretOrKey: config.secret
};


const jwtLogin = new JwtStrategy(jwtOptions, function(jwtPayload, done) {  
  User.findById(jwtPayload._id)
    .then(user => {
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    })
    .catch(err => done(err, false));
  })


passport.use(jwtLogin);  
passport.use(localLogin);  