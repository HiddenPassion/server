const express = require('express');
const passport = require('passport');

const AuthController = require('./controllers/authentication');
const BooksController = require('./controllers/books');
require('./setting/passportSetting');

const requireAuth = passport.authenticate('jwt', { session: false });  
const requireLogin = passport.authenticate('local', { session: false });  

module.exports = function(app) {  
  const apiRoutes = express.Router();
  const authRoutes = express.Router();
  const workWithBooksData = express.Router();

  
  apiRoutes.use('/auth', authRoutes); //+

    authRoutes.post('/signup', AuthController.register); //+

    authRoutes.post('/signin', requireLogin, AuthController.login);//+
    
  apiRoutes.use('/work-with-books-data', workWithBooksData); //+

    workWithBooksData.get('/books-list', BooksController.getBooks ); //+

    workWithBooksData.get('/book', BooksController.getBook); //+

    workWithBooksData.patch('/remove-book', requireAuth, BooksController.removeBook);//+

    workWithBooksData.patch('/take-book', requireAuth, BooksController.takeBook); //+

    workWithBooksData.patch('/return-book', requireAuth, BooksController.returnBook); //+

    workWithBooksData.get('/get-user-taken-books',  requireAuth, BooksController.getUserTakenBooks);

  app.use('/apiRequest', apiRoutes);

  //error handlers
  app.use((err, req, res, next) => {
    if (err.name === 'MongoError' && err.code === 11000){ 
      res.status(422).json({message : "User with  this email is yet exist", status: 422});
    } else {
      res.status(500).json(err);
    }
  });

  app.use((req, res) => {
    res.sendStatus(404);
  });

};
